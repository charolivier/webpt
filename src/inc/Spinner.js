import React from 'react';

const Spinner = function(props){
  return (
    <div className="spinner"></div>
  );
};

export default Spinner;