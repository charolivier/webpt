import React from 'react';
import Button from './Button.js';
import Player from './Player.js';

const Card = function(props){
  return (
    <div className="col-6">
      <div className="card my-5">
        <Player data={props.data} />
        <div className="card-footer">
          <Button onBtnClick={props.onBtnClick} />
        </div>
      </div>
    </div>
  );
};

export default Card;