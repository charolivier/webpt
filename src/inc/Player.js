import React from 'react';

const Err = function() {
  return <div className="card-body"><p>Bad request, try to reload..</p></div>;
}

const Suc = function(props) {
  const res = props.data.map((e)=>{
    return Object.keys(e).map((k)=>{
     return <li className="list-group-item" key={k}><span>{k}:</span> {e[k]}</li> 
    })
  });

  return (<ul className="list-group list-group-flush">{res}</ul>);
}

const Player = function(props){
  const res = (props.data && props.data.status === 'successful') ? <Suc data={props.data.payload} /> : <Err />;

  return (
    <div className="card-text">
      <h5 className="card-header">Featured</h5>
      {res}
    </div>
  );
};

export default Player;