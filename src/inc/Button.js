import React from 'react';

const Button = function(props){
  return <button className="btn btn-primary btn-block" onClick={props.onBtnClick}>Reload</button>;
}

export default Button;