import React, { Component } from 'react';
import Card from './inc/Card.js';
import Spinner from './inc/Spinner.js';

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response.json();
}

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      "res": null,
      "load": 1
    }
    
    this.getData = this.getData.bind(this);
  }

  getData(){
    this.setState({"load":1});
    
    fetch('http://interview.wptdev.com/api/killfeed')
      .then(handleErrors)
      .then(response => {
        this.setState({
          "res": response,
          "load": 0
        });
      })
      .catch(error => {
        this.setState({
          "res": null,
          "load": 0
        });
      });
   }
  
  componentDidMount() {
    this.getData();
  }

  render() {
    const res = (this.state.load) ? <Spinner /> : <Card onBtnClick={this.getData} data={this.state.res} />;

    return (
      <div className="App">
        <div className="container">
          <div className="row justify-content-center">
            {res}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
